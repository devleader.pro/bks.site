<?php
//test2
include_once 'includes/wp-reset-theme.php';
include_once 'includes/wp-api-menus.php';
include_once 'includes/wp-translate-slug.php';
include_once 'includes/wp-post-types.php';

// include_once 'includes/wp-meta-box.php';
include_once 'includes/wp-theme-options.php';
function vjwp_rest_theme_scripts() {
	$all_options = get_option('theme_options');
	$base_url  = esc_url_raw( home_url() );
	$base_path = rtrim( parse_url( $base_url, PHP_URL_PATH ), '/' );
		if ( defined( 'VJWP_VUE_DEV' ) && VJWP_VUE_DEV ) {
			wp_enqueue_script( 'rest-theme-vue', 'http://localhost:8080/dist/build.js', '1.0.0', true );
		} else {
			wp_enqueue_script( 'rest-theme-vue', get_template_directory_uri() . '/dist/build.js', '0.2.4', true );
		}
	
	wp_localize_script( 'rest-theme-vue', 'vjwp', array(
		'root'      => esc_url_raw( rest_url() ),
		'base_url'  => $base_url,
		'base_path' => $base_path ? $base_path . '/' : '/',
		'nonce'     => wp_create_nonce( 'wp_rest' ),
		'site_name' => get_bloginfo( 'name' ),
		'site_description' => get_bloginfo( 'description' ),
		'adress'	=> $all_options['adress'],
		'phone'		=> $all_options['phone'],
		'conf'		=> $all_options['conf'],
		'policy'		=> $all_options['policy'],
		'adv' 		=> array(
			'title' => $all_options['adv_title'],
			'desc' 	=> $all_options['adv_desc'],
			'adv1' 		=> array(
				'title' => $all_options['adv_title1'],
				'img' => $all_options['adv_img1'],
				'desc' => $all_options['adv_desc1'],
			),
			'adv2' 		=> array(
				'title' => $all_options['adv_title2'],
				'img' => $all_options['adv_img2'],
				'desc' => $all_options['adv_desc2'],
			),
			'adv3' 		=> array(
				'title' => $all_options['adv_title3'],
				'img' => $all_options['adv_img3'],
				'desc' => $all_options['adv_desc3'],
			),
		),
		'about_company' => $all_options['about_company'],
		'mission_company' => $all_options['mission_company'],
	) );
}
add_action( 'wp_enqueue_scripts', 'vjwp_rest_theme_scripts' );
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'top-menu' => __( 'Главное меню' ),
			'footer-menu-1' => __( 'О компании' ),
			'footer-menu-4' => __( 'Ракрытие информации' ),
		)
	);
}
add_filter( 'excerpt_more', '__return_false' );
add_action( 'after_setup_theme', 'vjwp_theme_setup' );
function vjwp_theme_setup() {
	add_theme_support( 'post-thumbnails' );
}
function vjwp_custom_rewrite_rule() {
	global $wp_rewrite;
	$wp_rewrite->front               = $wp_rewrite->root;
	$wp_rewrite->set_permalink_structure( '/%postname%/' );
	$wp_rewrite->page_structure      = $wp_rewrite->root . '%pagename%/';
	$wp_rewrite->author_base         = 'author';
	$wp_rewrite->author_structure    = '/' . $wp_rewrite->author_base . '/%author%';
	$wp_rewrite->set_category_base( 'category' );
	$wp_rewrite->set_tag_base( 'tag' );
	$wp_rewrite->add_rule( '^articles$', 'index.php', 'top' );
}
add_action( 'init', 'vjwp_custom_rewrite_rule' );
add_action( 'permalink_structure_changed', 'vjwp_forcee_perma_struct', 10, 2 );
function vjwp_forcee_perma_struct( $old, $new ) {
	update_option( 'permalink_structure', '/%postname%' );
}
add_filter( 'wp_title','vjwp_vue_title', 10, 3 );
function vjwp_vue_title( $title, $sep, $seplocation ) {
	if ( false !== strpos( $title, __( 'Page not found' ) ) ) {
		$replacement = ucwords( str_replace( '/', ' ', $_SERVER['REQUEST_URI'] ) );
		$title       = str_replace( __( 'Page not found' ), $replacement, $title );
	}
	return $title;
}
add_action( 'rest_api_init', 'vjwp_extend_rest_post_response' );
function vjwp_extend_rest_post_response() {
	register_rest_field( 'post',
		'featured_image_src',
		array(
			'get_callback'    => 'get_image_src',
			'update_callback' => null,
			'schema'          => null,
			 )
	);
	
	register_rest_field( 'post',
		'cat_name',
		array(
			'get_callback'    => 'vjwp_get_cat_name',
			'update_callback' => null,
			'schema'          => null,
			 )
	);
	register_rest_field( 'post',
		'tag_name',
		array(
			'get_callback'    => 'vjwp_get_tag_name',
			'update_callback' => null,
			'schema'          => null,
			)
	);
	register_rest_field( 'page',
		'featured_image_src',
		array(
			'get_callback'    => 'get_image_src',
			'update_callback' => null,
			'schema'          => null,
			 )
	);
	
	
}

function get_image_src( $object, $field_name, $request ) {
	$feat_img_array['full'] = wp_get_attachment_image_src( $object['featured_media'], 'full', false );
	$feat_img_array['medium'] = wp_get_attachment_image_src( $object['featured_media'], 'medium', false );
	$feat_img_array['thumbnail'] = wp_get_attachment_image_src( $object['featured_media'], 'thumbnail', false );
	$feat_img_array['srcset'] = wp_get_attachment_image_srcset( $object['featured_media'] );
	$image = is_array( $feat_img_array ) ? $feat_img_array : 'false';
	return $image;
}
function vjwp_get_cat_name( $object, $field_name, $request ) {
	$cats = $object['categories'];
	$res = [];
	$ob = [];
	foreach ( $cats as $x ) {
		$cat_id = (int) $x;
		$cat = get_category( $cat_id );
		if ( is_wp_error( $cat ) ) {
			$res[] = '';
		} else {
			$ob['name'] = isset( $cat->name ) ? $cat->name : '';
			$ob['id']   = isset( $cat->term_id ) ? $cat->term_id : '';
			$ob['slug'] = isset( $cat->slug ) ? $cat->slug : '';
			$res[] = $ob;
		}
	}
	return $res;
}
function vjwp_get_tag_name( $object, $field_name, $request ) {
	$tags = $object['tags'];
	$res = [];
	$ob = [];
	foreach ( $tags as $x ) {
		$tag_id = (int) $x;
		$tag = get_tag( $tag_id );
		if ( is_wp_error( $tag ) ) {
			$res[] = '';
		} else {
			$ob['name'] = isset( $tag->name ) ? $tag->name : '';
			$ob['id'] = isset( $tag->term_id ) ? $tag->term_id : '';
			$ob['slug'] = isset( $tag->slug ) ? $tag->slug : '';
			$res[] = $ob;
		}
	}
	return $res;
};

function slug_get_post_meta_cb( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name );
}

function slug_update_post_meta_cb( $value, $object, $field_name ) {
    return update_post_meta( $object[ 'id' ], $field_name, $value );
}

add_action( 'rest_api_init', 'slug_register_starship' );
function slug_register_starship() {
    register_rest_field( 'information',
        'values',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'page',
        'section_docs',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'page',
        'section_docs_title',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'page',
        'section_head',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'page',
        'section_text',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'page',
        'section_icon',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'page',
        'section_fields',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'reports',
        'fin_doc',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'reports',
        'fin_icon',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'reports',
        'fin_date_to',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
	register_rest_field( 'reports',
        'fin_date_create',
        array(
            'get_callback'    => 'slug_get_starship',
            'update_callback' => null,
            'schema'          => null,
        )
	);
}

function slug_get_starship( $object, $field_name, $request ) {
    return CFS()->get( $field_name, $object[ 'id' ] );
}
function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);