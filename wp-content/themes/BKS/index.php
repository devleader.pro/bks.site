<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body>

	<div id="content" class="site-content" >
	</div>

	<div id="app"></div>
	
	<?php wp_footer(); ?>
</body>
</html>

