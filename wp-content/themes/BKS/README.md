# Wordpress Theme with VueJS

## How to use?
1. Go to your WP theme directory (in `/wp-content/theme/`)
2. Clone / Download this repo
3. Activate your theme from WordPress theme's backend
4. This theme will display menu which has set display location to Primary Menu. 
5. Make sure you fulfill all the requirements before using theme. (See [Requirements](#requirements))

## Requirements
* WordPress Version 4.7+
