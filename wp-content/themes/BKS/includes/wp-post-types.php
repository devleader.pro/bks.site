<?php
    add_action('init', 'register_post_types');
    function register_post_types(){
        register_post_type('information', array(
            'label'  => 'Раскрытие информации',
            'labels' => array(
                'name'               => 'Раскрытие информации',
                'singular_name'      => 'Раскрытие информации',
                'add_new'            => 'Добавить раскрытие информации',
                'add_new_item'       => 'Добавление раскрытия информации',
                'edit_item'          => 'Редактирование раскрытия информации',
                'new_item'           => 'Новое раскрытие информации',
                'view_item'          => 'Смотреть раскрытие информации',
                'search_items'       => 'Искать раскрытие информации',
                'not_found'          => 'Не найдено',
                'not_found_in_trash' => 'Не найдено в корзине',
                'parent_item_colon'  => '',
                'menu_name'          => 'Раскрытие информации',
            ),
            'description'         => '',
            'public'              => true,
            'show_in_menu'        => true,
            'show_in_admin_bar'   => true,
            'show_in_rest'        => true,
            'rest_base'           => $post_type,
            'menu_position'       => '0',
            'menu_icon'           => null, 
            'hierarchical'        => false,
            'supports'            => array('title'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
            'taxonomies'          => array('infotype', 'finances'),
            'has_archive'         => true,
            'rewrite'             => true,
            'query_var'           => true,
        ) );
        register_post_type('reports', array(
            'label'  => 'Отчентность',
            'labels' => array(
                'name'               => 'Отчетности',
                'singular_name'      => 'Отчетность',
                'add_new'            => 'Добавить отчетность',
                'add_new_item'       => 'Добавление отчетности',
                'edit_item'          => 'Редактирование отчетности',
                'new_item'           => 'Новая отчетность',
                'view_item'          => 'Смотреть отчетность',
                'search_items'       => 'Искать отчетность',
                'not_found'          => 'Не найдено',
                'not_found_in_trash' => 'Не найдено в корзине',
                'parent_item_colon'  => '',
                'menu_name'          => 'Отчетность',
            ),
            'description'         => '',
            'public'              => true,
            'show_in_menu'        => true,
            'show_in_admin_bar'   => true,
            'show_in_rest'        => true,
            'rest_base'           => $post_type,
            'menu_position'       => '0',
            'menu_icon'           => null, 
            'hierarchical'        => true,
            'supports'            => array('title'),
            'taxonomies'          => array(),
            'has_archive'         => false,
            'rewrite'             => true,
            'query_var'           => true,
        ) );
        register_taxonomy('infotype', 'information',array(
            'label'  => 'Типы информации',
            'labels' => array(
                'name' => 'Типы информации',
                'singular_name' => 'Тип информации',
                'search_items' => 'Искать тип информации',
                'all_items' => 'Все типы информации',
                'parent_item' => 'Родительский тип информации',
                'parent_item_colon' => 'Родительский тип информации',
                'edit_item' => 'Редактировать тип информации',
                'update_item' => 'Обновить тип информации',
                'add_new_item' => 'Добавить новый тип информации',
                'new_item_name' => 'Новый тип информации',
                'menu_name' => 'Типы информации'
            ),
            'description'           => '', // описание таксономии
            'public'                => true,
            'show_in_rest'          => true, // добавить в REST API
            //'rest_base'             => $taxonomy,
            'hierarchical'          => true,
            'update_count_callback' => '',
            'rewrite'               => true,
            'capabilities'          => array(),
            'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
            'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
            '_builtin'              => false,
        ) );
        register_taxonomy('finances', 'reports',array(
            'label'  => 'Типы отчетности',
            'labels' => array(
                'name' => 'Типы отчетности',
                'singular_name' => 'Тип отчетности',
                'search_items' => 'Искать тип отчетности',
                'all_items' => 'Все типы отчетности',
                'parent_item' => 'Родительский тип отчетности',
                'parent_item_colon' => 'Родительский тип отчетности',
                'edit_item' => 'Редактировать тип отчетности',
                'update_item' => 'Обновить тип отчетности',
                'add_new_item' => 'Добавить новый тип отчетности',
                'new_item_name' => 'Новый тип отчетности',
                'menu_name' => 'Типы отчетности'
            ),
            'description'           => '', // описание таксономии
            'public'                => true,
            'show_in_rest'          => true, // добавить в REST API
            //'rest_base'             => $taxonomy,
            'hierarchical'          => true,
            'update_count_callback' => '',
            'rewrite'               => true,
            'capabilities'          => array(),
            'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
            'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
            '_builtin'              => false,
        ) );
    }