<?php
$site = 'allsite';
function theme_options() {
	global $site;
	add_menu_page( 'Параметры сайта', 'Параметры сайта', 'manage_options', $site, 'true_option_page', '',6);  
}
add_action('admin_menu', 'theme_options');
function true_option_page(){
	global $site;
	?><div class="wrap">
        <h2>Дополнительные параметры сайта</h2>
        <div>На данной странице вы можете настроить статические данные сайта. (Строку на баннере, на кнопке, адрес кнопки, адрес, телефон и т.п.)</div>
		<form method="post" enctype="multipart/form-data" action="options.php">
			<?php 
			settings_fields('theme_options');
			do_settings_sections($site);
			?>
			<p class="submit">  
				<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
			</p>
		</form>
	</div><?php
}
 
function true_option_settings() {
	global $site;
	register_setting( 'theme_options', 'theme_options', 'true_validate_settings' ); // theme_options
	add_settings_section( 'true_section_1', 'Настройки отображения сайта', '', $site );
	$true_field_params = array(
		'type'      => 'text',
		'id'        => 'adress',
		'desc'      => 'используется в шапке сайта', 
		'label_for' => 'adress' 
	);
	add_settings_field( 'adress', 'Адрес', 'true_option_display_settings', $site, 'true_section_1', $true_field_params );

    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'phone',
		'desc'      => 'используется в шапке сайта',
		'label_for' => 'phone'
	);
    add_settings_field( 'phone', 'Телефон', 'true_option_display_settings', $site, 'true_section_1', $true_field_params );
    
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'banner_title',
		'label_for' => 'banner_title'
	);
	add_settings_field( 'banner_title', 'Заголовок баннера', 'true_option_display_settings', $site, 'true_section_1', $true_field_params );

    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'banner_button',
		'label_for' => 'banner_button'
	);
	add_settings_field( 'banner_button', 'Текст кнопки на баннере', 'true_option_display_settings', $site, 'true_section_1', $true_field_params );
	


	$true_field_params = array(
		'type'      => 'text',
		'id'        => 'conf',
		'label_for' => 'conf'
	);
	add_settings_field( 'conf', 'Согласие на обработку персональных данных - ссылка на документ', 'true_option_display_settings', $site, 'true_section_1', $true_field_params );
	
	$true_field_params = array(
		'type'      => 'text',
		'id'        => 'policy',
		'label_for' => 'policy'
	);
	add_settings_field( 'policy', 'Политика в отношение обработи персональных данных - ссылка на документ', 'true_option_display_settings', $site, 'true_section_1', $true_field_params );
	


    add_settings_section( 'true_section_2', 'Преимущества (отображаются на глвной странице)', '', $site );
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_title',
		'label_for' => 'adv_title'
	);
	add_settings_field( 'adv_title', 'Заголовок', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
		'type'      => 'textarea',
        'id'        => 'adv_desc',
        'label_for' => 'adv_desc'
	);
	add_settings_field( 'adv_desc', 'Вводный текст', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );

    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_title1',
		'label_for' => 'adv_title1'
	);
	add_settings_field( 'adv_title1', 'Преимущество 1. Заголовок', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_img1',
		'label_for' => 'adv_img1'
	);
	add_settings_field( 'adv_img1', 'Преимущество 1. Картинка', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'adv_desc1',
        'label_for' => 'adv_desc1'
    );
	add_settings_field( 'adv_desc1', 'Преимущество 1. Текст', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_title2',
		'label_for' => 'adv_title2'
    );
    
	add_settings_field( 'adv_title2', 'Преимущество 2. Заголовок', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_img2',
		'label_for' => 'adv_img2'
	);
	add_settings_field( 'adv_img2', 'Преимущество 2. Картинка', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'adv_desc2',
        'label_for' => 'adv_desc2'
    );
    add_settings_field( 'adv_desc2', 'Преимущество 2. Текст', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
   
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_title3',
		'label_for' => 'adv_title3'
	);
	add_settings_field( 'adv_title3', 'Преимущество 3. Заголовок', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
		'type'      => 'text',
		'id'        => 'adv_img3',
		'label_for' => 'adv_img3'
	);
	add_settings_field( 'adv_img3', 'Преимущество 3. Картинка', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );
	
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'adv_desc3',
        'label_for' => 'adv_desc3'
    );
    add_settings_field( 'adv_desc3', 'Преимущество 3. Текст', 'true_option_display_settings', $site, 'true_section_2', $true_field_params );

    add_settings_section( 'true_section_3', 'О сайте', '', $site );
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'mission_company',
        'label_for' => 'mission_company'
    );
	add_settings_field( 'mission_company', 'Миссия и цели компании', 'true_option_display_settings', $site, 'true_section_3', $true_field_params );
	
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'about_company',
        'label_for' => 'about_company'
    );
	add_settings_field( 'about_company', 'О компании', 'true_option_display_settings', $site, 'true_section_3', $true_field_params );
	
}
add_action( 'admin_init', 'true_option_settings' );
 

function true_option_display_settings($args) {
	extract( $args );
 
	$option_name = 'theme_options';
 
	$o = get_option( $option_name );
 
	switch ( $type ) {  
		case 'text':  
			$o[$id] = esc_attr( stripslashes($o[$id]) );
			echo "<input class='regular-text' type='text' id='$id' name='" . $option_name . "[$id]' value='$o[$id]' />";  
			echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
		break;
		case 'textarea':  
			$o[$id] = esc_attr( stripslashes($o[$id]) );
			echo "<textarea class='code large-text' cols='50' rows='10' type='text' id='$id' name='" . $option_name . "[$id]'>$o[$id]</textarea>";  
			echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
		break;
		case 'checkbox':
			$checked = ($o[$id] == 'on') ? " checked='checked'" :  '';  
			echo "<label><input type='checkbox' id='$id' name='" . $option_name . "[$id]' $checked /> ";  
			echo ($desc != '') ? $desc : "";
			echo "</label>";  
		break;
		case 'select':
			echo "<select id='$id' name='" . $option_name . "[$id]'>";
			foreach($vals as $v=>$l){
				$selected = ($o[$id] == $v) ? "selected='selected'" : '';  
				echo "<option value='$v' $selected>$l</option>";
			}
			echo ($desc != '') ? $desc : "";
			echo "</select>";  
		break;
		case 'radio':
			echo "<fieldset>";
			foreach($vals as $v=>$l){
				$checked = ($o[$id] == $v) ? "checked='checked'" : '';  
				echo "<label><input type='radio' name='" . $option_name . "[$id]' value='$v' $checked />$l</label><br />";
			}
			echo "</fieldset>";  
		break; 
	}
}
 
function true_validate_settings($input) {
	foreach($input as $k => $v) {
		$valid_input[$k] = trim($v);
	}
	return $valid_input;
}