<?php
    remove_action('wp_head','feed_links_extra',3);
    remove_action('wp_head','feed_links',2);
    remove_action('wp_head','rsd_link');
    remove_action('wp_head','wlwmanifest_link');
    remove_action('wp_head','start_post_rel_link',10,0);
    remove_action('wp_head','index_rel_link');
    remove_action('wp_head','adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head','wp_shortlink_wp_head', 10,0);
    // remove_action('wp_head','rest_output_link_wp_head');
    remove_action('wp_head','wp_oembed_add_discovery_links');
    remove_action('wp_head','dns-prefetch', 11, 0);
    remove_action('wp_head','print_emoji_detection_script', 7);
    remove_action('wp_head','wp_resource_hints', 2);
    remove_action('wp_head','wp_print_scripts');
    remove_action('wp_head','wp_print_head_scripts',9);
    remove_action('wp_head','wp_enqueue_scripts',9);
    remove_action('template_redirect','rest_output_link_header', 11, 0);
    remove_action('wp_head', 'wp_print_styles',8);
    wp_deregister_script('jquery');
    add_filter('the_generator','__return_false');
    add_filter('show_admin_bar','__return_false');
    function my_deregister_scripts(){
        wp_deregister_script( 'wp-embed' );
    }
    add_action('wp_footer','wp_enqueue_scripts',9);
    add_action( 'wp_footer', 'my_deregister_scripts' );

    // function remove_x_pingback($headers) {
    //     unset($headers['X-Pingback']);
    //     return $headers;
    // }
    // add_filter('wp_headers', 'remove_x_pingback');