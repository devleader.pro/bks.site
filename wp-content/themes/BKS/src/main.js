import "babel-polyfill";
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { HTTP as axios } from './axios/axios';
import YmapPlugin from 'vue-yandex-maps';

Vue.use( Vuex );
Vue.use( VueRouter );
Vue.use( YmapPlugin );
Vue.config.debug = true;
Vue.config.devTools = true;
Vue.prototype.$http = axios;

import App from './App.vue';
import Home from './pages/Home/Home.vue';
import NotFound from './pages/NotFound/NotFound.vue';
import Page from './pages/Page/Page.vue';
import Information from './pages/Information/Information.vue';
import Finance from './pages/Finance/Finance.vue';
import Service from './pages/Service/Service.vue';
import News from './pages/News/News.vue';
import Managment from './pages/Managment/Managment.vue';
import Brokerige from './pages/Brokerige/Brokerige.vue';
import Agent from './pages/Agent/Agent.vue';
import header from './components/PageHeader/PageHeader.vue';
import footer from './components/PageFooter/PageFooter.vue';
import Banner from './components/Banner/Banner.vue';
import Advantages from './components/Advantages/Advantages.vue';
import About from './components/About/About.vue';
import Mission from './components/Mission/Mission.vue';
import Services from './components/Services/Services.vue';
import LeftMenu from './components/LeftMenu/LeftMenu.vue';
import ContactForm from './components/ContactForm/ContactForm.vue';

Vue.component( 'App', App );
Vue.component( 'Home', Home );
Vue.component( 'NotFound', NotFound );
Vue.component( 'Page', Page );
Vue.component( 'News', News );
Vue.component( 'Service', Service );
Vue.component( 'Information', Information );
Vue.component( 'Finance', Finance );
Vue.component( 'Managment', Managment );
Vue.component( 'Brokerige', Brokerige );
Vue.component( 'Agent', Agent );
Vue.component( 'PageHeader', header );
Vue.component( 'PageFooter', footer );
Vue.component( 'Banner', Banner );
Vue.component( 'Advantages', Advantages );
Vue.component( 'About', About );
Vue.component( 'Mission', Mission );
Vue.component( 'Services', Services );
Vue.component( 'LeftMenu', LeftMenu );
Vue.component( 'ContactForm', ContactForm );

const router = new VueRouter( {
	mode: 'history',
	routes: [
		{ path: '/about', component: Page },
		{ path: '/about/news/', component: News },
		{ path: '/about/:name', component: Page },
		{ path: '/management', component: Managment },
		{ path: '/brokerige', component: Brokerige },
		{ path: '/agent', component: Agent },
		{ path: '/management/:name/', component: Managment },
		{ path: '/brokerige/:name/', component: Brokerige },
		{ path: '/agent/:name/', component: Agent },
		{ path: '/information/open', component: Information },
		{ path: '/information/finance/', redirect: '/information/finance/month' },
		{ path: '/information/open/:name', component: Information },
		{ path: '/information/finance/:name', component: Finance },
		{ path: '/information', redirect: '/information/open' },
		{ path: '/information/:name', component: Page },
		{ path: '/finances', redirect: '/information/finance/month' },
		{ path: '/finances/:name', redirect: '/information/finance/:name' },
		{ path: '/infotype', redirect: '/information/open' },
		{ path: '/infotype/:name', redirect: '/information/open/:name' },
		{ path: '/', component: Home }, 	
		{ path: '/404', component: NotFound }, 
		{ path: '*', redirect: '/404' }
	],
	scrollBehavior( to, from, savedPosition ) {
		const value = ( to.hash ) ? { selector: to.hash	} : { x: 0, y: 0 };
		return value;
	}
} );
const store = new Vuex.Store( {
	state: {
		title: ''
	},
	mutations: {
		rtChangeTitle( state, value ) {
			state.title = value;
			document.title = ( state.title ? state.title + ' - ' : '' ) + vjwp.site_name;
		}
	}
} );

new Vue( {
	router,
	store,
	render: createElement => createElement( App ),
} ).$mount( '#app' );
