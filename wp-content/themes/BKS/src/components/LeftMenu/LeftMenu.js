export default {
	mounted() {
		this.getMenu();
	},
	props: {
		dataTransfer: {
			type: Object,
		}
	},
	data() {
		return {
			data: this.dataTransfer,
			leftMenu: {},
		};
	},
	methods: {
		getMenu: function() {
			for ( let index = 0; index < this.data.mainMenu.length; index++ ) {
				const curMenu = this.data.mainMenu[ index ];
				if ( curMenu.url.split( '/' )[ 0 ] && curMenu.url.split( '/' )[ 0 ] != '#' ) {
					const urlPath = ( curMenu.url.split( '/' )[ 3 ] ) ? curMenu.url.split( '/' )[ 3 ].toString() : curMenu.url.split( '/' )[ 1 ].toString();
					if ( urlPath === this.$route.path.split( '/' )[ 1 ].toString() ) {
						this.leftMenu = curMenu;
					}
				}
			}
		},
		getUrlName: function( url ) {
			if ( url && url != '' && url != '#' ) {
				const array = url;
				return array.replace( vjwp.base_url, '' );
			}
		},
	},
	watch: {
		'$route'( to, from ) {
			this.getMenu();
		}
	}
};
