let menu;
export default {
	name: 'Adventages',
	data() {
		return {
			advTitle: vjwp.adv.title,
			advDesc: vjwp.adv.desc,
			advs: [
				{
					id: '0',
					title: vjwp.adv.adv1.title,
					img: vjwp.adv.adv1.img,
					desc: vjwp.adv.adv1.desc,
				},
				{
					id: '1',
					title: vjwp.adv.adv2.title,
					img: vjwp.adv.adv2.img,
					desc: vjwp.adv.adv2.desc,
				},
				{
					id: '2',
					title: vjwp.adv.adv3.title,
					img: vjwp.adv.adv3.img,
					desc: vjwp.adv.adv3.desc,
				},
			]
		};
	},
	
};
