export default {
	name: 'Services',
	mounted: function() {
		const vm = this;
		vm.getPage();
	},
	
	data() {
		return {
			page: {},
			loaded: 'false',
			pageTitle: ''
		};
	},
	methods: {
		getPage: function() {
			const vm = this;
			vm.loaded = 'false';

			vm.$http.get( 'wp/v2/pages?slug=brokerige,agent,management&order=asc&parent=0' )
			.then( ( res ) => {
				vm.page = res.data;
				vm.loaded = 'true';
			} )
			.catch( ( res ) => {
			} );

		}
	},
	watch: {
		'$route'( to, from ) {
			this.getPage();
		}
	}
	
};
