let menu;
export default {
	name: 'PageHeader',
	props: {
		dataTransfer: {
			type: Object,
		}
	},
	data() {
		return {
			data: this.dataTransfer
		};
	},
	methods: {
		getUrlName: function( url ) {
			const array = url;
			return array.replace( vjwp.base_url, '' );
		},
		
		menuOpen() {
			const mobileMenu = document.querySelector( "#menu" );
			const bodyEl = document.querySelector( "body" );
			mobileMenu.classList.toggle( 'active' );
			bodyEl.classList.toggle( 'no-scroll' );
		},
		menuClose() {
			const mobileMenu = document.querySelector( "#menu" );
			const bodyEl = document.querySelector( "body" );
			mobileMenu.classList.remove( 'active' );
			bodyEl.classList.remove( 'no-scroll' );
		}
	},
	watch: {
		'$route'( to, from ) {
			this.menuClose();
		}
	}
};
