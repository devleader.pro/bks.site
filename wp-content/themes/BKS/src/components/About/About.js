let menu;
export default {
	mounted: function() {
		this.getPage();
	},
	data() {
		return {
			about: vjwp.about_company,
			data: [],
			readyData: 'false'
		};
	},
	methods: {
		getPage: function() {
			const vm = this;
			vm.$http.get( 'wp/v2/pages', {
				params: { slug: 'licenses' }
			} )
			.then( ( res ) => {
				vm.data = res.data[ 0 ].section_docs;
				vm.readyData = 'true';
			} )
			.catch( ( res ) => {
			} );
		}
	}
};
