import {mapState,mapGetters,mapMutations} from 'vuex'
export default {
	props: {
		contactForm: {
			type: String
		}
	},
	matched: function () {
		this.openForm();
	},
	data() {
		return {
			fio: '',
			phone: '',
			email: '',
			check: false,
			open: 'false'
		};
	},
	methods:{
		closeForm: function() {
			this.open = 'false';
		},
		openForm: function() {
			this.open = 'true';
		}
	},
	watch: {
		contactForm: function(){
			if(this.contactForm == 'true'){
				this.openForm()
			} else {
				this.closeForm()

			}
		},
		
	}
	
};
