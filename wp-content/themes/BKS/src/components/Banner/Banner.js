export default {
	data() {
		return {
			contactForm: 'false',
			fio: '',
			phone: '',
			email: '',
			content: '',
			check: false,
			open: 'false',
			formFields: [],
			formFieldsReady: 'false'
		};
	},
	methods: {
		openForm() {
			this.contactForm = 'true';
		},
		closeForm() {
			this.contactForm = 'false';
		},
		postForm: function() {
			this.$http.post( 'wp/v2/comments/', {
				post: '404',
				author_email: this.email,
				author_name: this.fio,
				content: `${ this.fio }, ${ this.email }, ${ this.phone }, ${ this.content }`
			}, {
				headers: {
					'X-WP-Nonce': vjwp.nonce,
				},
			} );
		}
	}
};
