export default {
	data: function() {
		return {
			data: {
				pages: {},
				pagesReady: 'false',
				news: {},
				newsReady: 'false',
				infotypes: {},
				infotypesReady: 'false',
				information: {},
				informationReady: 'false',
				finances: {},
				financesReady: 'false',
				reports: {},
				reportsReady: 'false',
				mainMenu: [],
				mainMenuReady: 'false',
				footerMenuLeft: [],
				footerMenuLeftReady: 'false',
				footerMenuRight: [],
				footerMenuRightReady: 'false',
				siteName: vjwp.site_name,
				address: vjwp.adress,
				phone: vjwp.phone,
				
			}
		};
	},
	mounted: function() {
		this.getPages();
		this.getInfotypes();
		this.getInformation();
		this.getReports();
		this.getNews();
		this.getFinances();
		this.getReports();
		this.getMainMenu();
		this.getFooterMenuRight();
		this.getFooterMenuLeft();
	},
	methods: {
		getFooterMenuLeft: function() {
			this.data.footerMenuLeftReady = 'false';
			this.$http.get( 'wp-api-menus/v2/menu-locations/footer-menu-1' )
			.then( ( res ) => {
				this.data.footerMenuLeft = res.data;
				this.data.footerMenuLeftReady = 'true';
			} );
		},
		getFooterMenuRight: function() {
			this.data.footerMenuRightReady = 'false';
			this.$http.get( 'wp-api-menus/v2/menu-locations/footer-menu-4' )
			.then( ( res ) => {
				this.data.footerMenuRight = res.data;
				this.data.footerMenuRightReady = 'true';
			} );
		},
		getMainMenu: function() {
			this.data.mainMenuReady = "false";
			this.$http.get( 'wp-api-menus/v2/menu-locations/primary-menu' )
			.then( ( res ) => {
				this.data.mainMenu = res.data;
				this.data.mainMenuReady = "true";
			} );
	
		},
		getFinances: function() {
			this.data.financesReady = 'false';
			this.$http.get( 'wp/v2/finances', {
				params: {
					order: 'asc',
					orderby: 'id',
				}
			} )
			.then( ( res ) => {
				this.data.finances = res.data;
				this.data.financesReady = 'true';
				
			} );
		},
		getReports: function() {
			this.data.reportsReady = 'false';
			this.$http.get( 'wp/v2/reports', {
				params: {
					order: 'desc',
					per_page: 100,
				}
			} )
			.then( ( res ) => {
				this.data.reports = res.data;
				this.data.reportsReady = 'true';
			} );
		},
		getInfotypes: function() {
			this.data.infotypesReady = 'false';
			this.$http.get( 'wp/v2/infotype', {
				params: {
					order: 'asc',
					orderby: 'id',
				}
			} )
			.then( ( res ) => {
				this.data.infotypes = res.data;
				this.data.infotypesReady = 'true';
			} );
		},
		getInformation: function() {
			this.data.informationReady = 'false';
			this.$http.get( 'wp/v2/information', {
				params: {
					order: 'asc',
					per_page: 100,
				}
			} )
			.then( ( res ) => {
				this.data.information = res.data;
				this.data.informationReady = 'true';
			} );
		},
		getPages: function() {
			this.data.pagesReady = 'false';
			this.$http.get( 'wp/v2/pages', {
				params: {
					per_page: 100
				}
			} )
			.then( ( res ) => {
				this.data.pages = res.data;
				this.data.pagesReady = 'true';
			} );
		},
		getNews: function() {
			this.data.newsReady = 'false';
			this.$http.get( 'https://newsapi.org/v2/top-headlines', {
				params: { 
					country: 'ru',  
					category: 'business', 
					apiKey: '9af01dbbf61542438598966be04d98f6',
					pageSize: 10,
				}
			} )
			.then( ( res ) => {
				this.data.news = res.data.articles;
				this.data.newsReady = 'true';
			} );
		}
	}
};
