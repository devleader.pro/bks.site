import getPage from '../../mixins/getPage.js';

export default {
	mixins: [ 
		getPage 
	],
	mounted: function() {
		this.getPage();
		this.getNews();
	},
	props: {
		dataTransfer: {
			type: Object
		}
	},
	data() {
		return {
			page: {},
			load: 'false',
			pageTitle: '',
			site_name: vjwp.site_name,
			address: vjwp.adress,
			phone: vjwp.phone,
			map: 'false',
			data: this.dataTransfer
		};
	},
	methods: {
		getNews: function() {
			this.page.articles = this.data.news;
		},
		getDate: function( dateValue ) {
			const date = dateValue.split( 'T' )[ 0 ];
			const dateArr = date.split( '-' );
			return dateArr[ 2 ] + '.' + dateArr[ 1 ] + '.' + dateArr[ 0 ];
		}
	},
	watch: {
		'$route'( to, from ) {
			this.getPage();
		}
	}
};
