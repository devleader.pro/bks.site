import getPage from '../../mixins/getPage.js';

export default {
	mixins: [ 
		getPage 
	],
	mounted: function() {
		this.getPage();
		this.getContent();
	},
	props: {
		dataTransfer: {
			type: Object
		}
	},
	data() {
		return {
			page: {},
			load: false,
			content: {},
			contentReady: 'false',
			loaded: 'false',
			pageTitle: '',
			data: this.dataTransfer
		};
	},
	methods: {
		getContent: function() {
			const vm = this;
			vm.contentReady = 'false';
			vm.$http.get( 'wp/v2/finances', {
				params: {
					order: 'asc',
					orderby: 'id',
					slug: ( this.$route.params.name != 'information' ) ? this.$route.params.name : ''
				}
			} )
			.then( ( res ) => {
				vm.content = res.data;
				let loadedIndex = 0;
				for ( let index = 0; index < vm.content.length; index++ ) {
					vm.content[ index ].content = {};
					vm.$http.get( 'wp/v2/reports', {
						params: {
							order: 'desc',
							per_page: 100,
							finances: vm.content[ index ].id
						}
					} )
					.then( ( subres ) => {
						vm.content[ index ].content = subres.data;
						if ( ++loadedIndex == vm.content.length ) {
							vm.contentReady = 'true';
						}
					} );
				}
			} );
		},
		getDate( date, end_date ) {
			const start_m = Date.parse( date );
			const end_temp = ( end_date ) ? `${ end_date.split( '.' )[ 2 ] }-${ end_date.split( '.' )[ 1 ] }-${ end_date.split( '.' )[ 0 ] }` : '';

			const end_m = ( end_temp != '' ) ? Date.parse( end_temp ) : ( Date.parse( date ) + 94608000000 );
			const start = new Date( start_m );
			const end = new Date( end_m );
			const today = Date.parse( new Date() ) < end_m;
			
			return {
				start: `${ ( start.getDate() < 10 ) ? '0' + start.getDate() : start.getDate() }.${ ( start.getMonth() + 1 < 10 ) ? '0' + ( start.getMonth() + 1 ) : start.getMonth() + 1 }.${ start.getFullYear() }`,
				end: `${ ( end.getDate() < 10 ) ? '0' + end.getDate() : end.getDate() }.${ ( end.getMonth() + 1 < 10 ) ? '0' + ( end.getMonth() + 1 ) : end.getMonth() + 1 }.${ end.getFullYear() }`,
				today: today
			};
		}
	},
	watch: {
		'$route'( to, from ) {
			this.getPage();
			this.getContent();
		}
	}
};
