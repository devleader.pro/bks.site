import getPage from '../../mixins/getPage.js';

export default {
	mixins: [ 
		getPage 
	],
	mounted: function() {
		this.getPage();
	},
	props: {
		dataTransfer: {
			type: Object
		},
		
	},
	data() {
		return {
			page: {},
			site_name: vjwp.site_name,
			address: vjwp.adress,
			phone: vjwp.phone,
			load: 'false',
			map: 'false',
			data: this.dataTransfer,
			slug: '',
		};
	},

	watch: {
		'$route'( to, from ) {
			this.getPage();

		}
	}
};
