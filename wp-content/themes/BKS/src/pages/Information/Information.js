import getPage from '../../mixins/getPage.js';

export default {
	mixins: [ 
		getPage 
	],
	mounted: function() {
		this.getPage();
		this.getItems();
		this.getResults();
	},
	props: {
		dataTransfer: {
			type: Object
		}
	},
	data: function() {
		return {
			query: '',
			page: {},
			load: 'false',
			content: [],
			readyContent: 'false',
			results: [],
			resultsReady: 'false',
			pageTitle: '',
			data: this.dataTransfer,
			arrMap: {}
		};
	},
	methods: {
		getItems() {
			this.readyContent = 'false';
			this.content = [];
			for ( let index = 0; index < this.data.infotypes.length; index++ ) {
				const cat = this.data.infotypes[ index ];
				cat.content = [];
				this.arrMap[ `id${ cat.id }` ] = { i: '', s: cat.slug };
				if ( ( this.$route.params.name == undefined || this.$route.params.name == 'open' ) || 
					( cat.slug == this.$route.params.name ) || 
					( cat.slug + 'f' == this.$route.params.name )
				) {
					this.content.push( cat );
					this.arrMap[ `id${ cat.id }` ].i = this.content.length - 1;
				}
			}
			for ( let index = 0; index < this.data.information.length; index++ ) {
				const el = this.data.information[ index ];
				if ( el.infotype[ 0 ] && el.infotype[ 0 ] != undefined ) {
					if ( this.content[ this.arrMap[ `id${ el.infotype[ 0 ] }` ].i ] != undefined ) {
						this.content[ this.arrMap[ `id${ el.infotype[ 0 ] }` ].i ].content.push( el );
					}
				} 
			}
			this.readyContent = 'true';
		},
		getResults: function( q ) {
			const vm = this;
			this.resultsReady = 'false';
			this.results = [];
			
			if ( q && q != '' ) {
				for ( let index = 0; index < vm.content.length; index++ ) {
					const ready = {
						cat: false,
						name: false,
						value: false,
					};
					const catTemp = JSON.stringify( vm.content[ index ] );
					const cat = JSON.parse( catTemp );
					const resCat = [];
					if ( cat.description.indexOf( q ) > -1 ) {
						cat.description = '';
						cat.description = vm.content[ index ].description.replace( q, '<span class="select">' + q + "</span>" );
						resCat[ index ] = cat;
						ready.cat = true;
					}
					if ( ready.cat == true ) {
						vm.results[ index ] = cat;
					}
					for ( let i = 0; i < cat.content.length; i++ ) {
						const itemTemp = JSON.stringify( vm.content[ index ].content[ i ] );
						const item = JSON.parse( itemTemp );
						const resItems = [];
						if ( item.title.rendered.indexOf( q ) > -1 ) {
							item.title.rendered = '';
							item.title.rendered = vm.content[ index ].content[ i ].title.rendered.replace( q, '<span class="select">' + q + "</span>" );
							if ( ready.cat == false ) {
								vm.results[ index ] = cat;
								ready.cat == true;
								vm.results[ index ].content = [];
							}
							vm.results[ index ].content[ i ] = item;
							ready.name = true;
						} 
						if ( ready.name == false && ready.cat == true ) {
							vm.results[ index ].content = vm.content[ index ].content;
							ready.name = true;
						}
						if ( item.values && item.values.length > 0 ) {
							const valuesTemp = JSON.stringify( vm.content[ index ].content[ i ].values );
							const values = JSON.parse( valuesTemp );
							const valueItems = [];
							for ( let val = 0; val < values.length; val++ ) {
								if ( item.values[ val ] ) {
									const valueClean = item.values[ val ].value.replace( /<\/?[^>]+>/, '' );
									if ( valueClean.indexOf( q ) > -1 ) {
										ready.value = true;
										const value = item.values[ val ].value.replace( q, '<span class="select">' + q + "</span>" );
										if ( ready.cat == false && ready.value == true ) {
											vm.results[ index ] = cat;
											ready.cat = true;
											vm.results[ index ].content = [];
										}
										if ( ready.name == false && ready.value == true ) {
											vm.results[ index ].content[ i ] = item;
											ready.name = true;
										}
										if ( ready.name == true && ready.value == true && vm.results[ index ].content[ i ] && ! vm.results[ index ].content[ i ].values ) {
											vm.results[ index ].content[ i ].values = [];
											const tempVal = JSON.stringify( vm.page[ index ].content[ i ].values[ val ] ).replace( q, '<span class="select">' + q + "</span>" );
											vm.results[ index ].content[ i ].values[ val ] = JSON.parse( tempVal );
											vm.results[ index ].content[ i ].values[ val ].value = value;
										}
										
									}
								}
							}
						}
					}
				}
				
				this.resultsReady = 'true';
			} else {
				this.results = this.content;
				this.resultsReady = 'true';
			}
		}
	},
	watch: {
		'$route'( to, from ) {
			this.getPage();
			this.getItems();
			this.getResults();
			this.query = '';
		},
		query: function() {
			this.getResults( this.query );
			
		}
	}
};
