import getPage from '../../mixins/getPage.js';

export default {
	mixins: [ 
		getPage 
	],
	mounted: function() {
		this.getPage();
	},
	props: {
		dataTransfer: {
			type: Object
		}
	},
	data() {
		return {
			page: {},
			data: this.dataTransfer,
			load: 'false'
		};
	},
	watch: {
		'$route'( to, from ) {
			this.getPage();
		}
	}
};
