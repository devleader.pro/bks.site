export default {
	data() {
		return {
			pageTitle: 'Главная'
		};
	},
	methods: {
		formatDate: function( value ) {
			value = value.date;
			if ( value ) {
				const date = new Date( value );
				const monthNames = [ "января", "февраля", "марта",
					"апреля", "мая", "июня", "июля",
					"августа", "сентября", "октября",
					"ноября", "декабря" ];
				const day = date.getDate();
				const monthIndex = date.getMonth();
				const year = date.getFullYear();
				return day + ' ' + monthNames[ monthIndex ] + ' ' + year;
			}
		}
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
