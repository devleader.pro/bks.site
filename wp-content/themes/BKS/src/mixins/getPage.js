export default {
	name: 'getPage',
	methods: {
		getPage: function() {
			this.load = 'false';
			const path = this.$route.path.split( '/' );
			const slug = ( path[ path.length - 1 ] != '' ) ? ( path[ path.length - 1 ] ) : ( path[ path.length - 2 ] );
			const data = this.data.pages.filter( page => page.slug == slug )[ 0 ];
			this.page = this.data.pages.filter( page => page.slug == slug )[ 0 ];
			this.$store.commit( 'rtChangeTitle', this.page.title.rendered );
			this.map = ( slug == 'contacts' ) ? 'true' : 'false';
			this.slug = slug;
			this.load = 'true';

		}
	},
}; 
